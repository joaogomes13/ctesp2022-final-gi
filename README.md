# Requisitos

## API deve retornar

### `Total em crédito`

- Total de saldo na conta, ou seja, valor positivo que poderá ser gasto pelo
  utilizador da conta

### `Total em Débito`

- Total em débito na conta, ou seja, valor em dívida que o utilizador irá
  posteriormente ser descontado no valor em crédito;

### `Saldo contabilístico`

- é a diferença entre o crédito e o débito

### `Balanço Diário`

- neste campo deve ser possível verificar o total gasto e o total recebido na conta por cliente;

## Requisitos funcionais

- Uma conta bancária será associada unicamente a um cliente
- Um cliente poderá ter várias contas bancárias

## Entidades

### `Client`

- Id
- Name
- Address
- Contact

### `Bank Account`

- Id
- Client Id
- Number
- IBAN
- balance

### `Transaction`

- Id
- Bank Account Id
- day
- value
- Transaction Type Id

### `Transaction Type`

- Id
- Client Id
- Number
- IBAN
- balance

## End-Points

### `BankAccount/GET/{id}`

O Id corresponderá ao id da conta bancária;

- Deve retornar:
  - Nome do Titular
  - Morada
  - Contacto
  - Saldo Atual

### `BankAccount/Post` 

Deve adicionar informações sobre transação:

- Número da conta
- Data de Transação
- Valor da transação
- Tipo de transação (Débito/Crédito)



### `BankAccount/Get/{id}/transactions`

Deve retornar dados das transações tais com o Json abaixo

- Array de transações dos últimos 30 dias (Exemplo: Se o pedido for feito a
  04/01/2020, o endpoint deve retornar dados desde 04/12/2019 até
  04/01/2020)
  - Mostra o saldo de cada dia apartir das transações realizadas naquele dia Debito ou Credito
  - Data
  - Balanço (Saldo à data)
  - Total Créditos em euros naquele dia
  - Total de Débitos em euros naquele dia

```json
    "transactions" : [
      {
        "date" : "2015-03-01T00:00:00",
        "total_incomings" : 200,
        "total_outcomings" : 100,
        "balance" : 11000
      },
      {
        "date" : "2015-03-01T00:00:00",
        "total_incomings" : 300,
        "total_outcomings" : 100,
        "balance" : 11000
      },
    ]
```

## Processo de CI / CD

![Processo CI CD](https://i.imgur.com/4dQlWRT.png)


## Testes unitários 

- BankAccount/Get/{id}/Transação
