using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Models
{
  public class Transaction
  {
    [Key]
    public int id { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public int bankAccountId { get; set; }
    public BankAccount bankAccount { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [DataType(DataType.DateTime)]
    public string day { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [Range(1, int.MaxValue, ErrorMessage = "O valor deve ser maior que zero")]
    public float value { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [Range(1, int.MaxValue, ErrorMessage = "Transaction Type inválido")]
    public int transactionTypeid { get; set; }
    public TransactionType transactionType { get; set; }

  }
}