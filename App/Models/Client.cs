using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Models
{
  public class Client
  {
    [Key]
    public int id { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [MaxLength(60, ErrorMessage = "Eeste campo deve conter entre 2 e 60 caracteres")]
    [MinLength(2, ErrorMessage = "Eeste campo deve conter entre 2 e 60 caracteres")]
    public string name { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [MaxLength(60, ErrorMessage = "Eeste campo deve conter entre 6 e 60 caracteres")]
    [MinLength(6, ErrorMessage = "Eeste campo deve conter entre 6 e 60 caracteres")]
    public string address { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [MaxLength(30, ErrorMessage = "Eeste campo deve conter entre 2 e 60 caracteres")]
    [MinLength(2, ErrorMessage = "Eeste campo deve conter entre 2 e 60 caracteres")]
    public string contact { get; set; }
    public List<BankAccount> bankAccounts { get; set; }



  }
}