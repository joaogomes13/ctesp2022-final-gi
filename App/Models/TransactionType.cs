using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Models
{
  public class TransactionType
  {
    [Key]
    public int id { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public string type { get; set; }

  }
}