using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Models
{
  public class BankAccount
  {
    [Key]
    public int id { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public int clientId { get; set; }
    public Client client { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    public int number { get; set; }

    [Required(ErrorMessage = "Este campo é obrigatório")]
    [MaxLength(25, ErrorMessage = "Este campo deve conter 25 caracteres")]
    [MinLength(25, ErrorMessage = "Este campo deve conter 25 caracteres")]
    public string iban { get; set; }
    public float balance { get; set; }

    public List<Transaction> transactions { get; set; }



  }
}