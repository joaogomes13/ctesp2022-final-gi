using Microsoft.EntityFrameworkCore;
using App.Models;
using System;

namespace App.Contexts
{
  public class DatabaseContext : DbContext
  {

    public DatabaseContext()
    {
    }

    public DbSet<Client> clients { get; set; }
    public DbSet<BankAccount> bankAccounts { get; set; }
    public DbSet<Transaction> transactions { get; set; }
    public DbSet<TransactionType> transactionTypes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      try
      {
        optionsBuilder.UseSqlServer(@"Server=app_database_1;Database=dotfinances;User Id=sa;Password=Docker1234!");
      }
      catch (System.Exception)
      {

        Console.WriteLine("Connection");
      }


    }
  }
}