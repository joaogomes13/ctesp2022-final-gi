using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using App.Dtos;
using App.Service;

namespace App.Services
{
  public class BankAccountService

  {

    public BankAccountService()
    {

    }


    public async Task<List<BankAccount>> GetAll(
      DatabaseContext context
    )
    {
      var accounts = await context.bankAccounts
        .Include(x => x.client)
        .Include(x => x.transactions)
        .AsNoTracking()
        .ToListAsync();

      return accounts;

    }

    public async Task<BankAccount> GetById(
      DatabaseContext context,
      int id
    )
    {
      var bankAccount = await context.bankAccounts
      .Include(x => x.client)
      .Include(x => x.transactions)
      .AsNoTracking()
      .FirstOrDefaultAsync(x => x.id == id);

      return bankAccount;
    }

    public async Task<BankAccountGetResponse> Get(
      DatabaseContext context,
      int id
    )
    {
      var bankAccount = await context.bankAccounts
      .Include(x => x.client)
      .Include(x => x.transactions)
      .AsNoTracking()
      .FirstOrDefaultAsync(x => x.id == id);

      BankAccountGetResponse bankAccountResponse = new BankAccountGetResponse(
        bankAccount.client.name,
        bankAccount.client.address,
        bankAccount.client.contact,
        bankAccount.balance
      );


      return bankAccountResponse;
    }

    public async Task<List<BankAccount>> GetByClient(
      DatabaseContext context,
      int id
   )
    {
      var accounts = await context.bankAccounts
        .Include(x => x.client)
        .AsNoTracking()
        .Where(x => x.clientId == id)
        .ToListAsync();

      return accounts;
    }

    public async Task<BankAccount> Save(
       DatabaseContext context,
       BankAccount bankAccount
      )
    {
      ClientService clientService = new ClientService();

      try
      {
        var expectedClient = await clientService.GetById(
          context,
          bankAccount.clientId
        );

      }
      catch (System.Exception)
      {

        throw;
      }


      context.bankAccounts.Add(bankAccount);
      await context.SaveChangesAsync();
      return bankAccount;


    }


    public async Task<BankAccountPostResponse> Post(
       DatabaseContext context,
       Transaction newTransaction
      )
    {


      TransactionService transactionService = new TransactionService();
      BankAccountService bankAccountService = new BankAccountService();
      TransactionTypeService transactionType = new TransactionTypeService();


      var transaction = await transactionService.Save(
        context,
        newTransaction
      );

      var bankAccount = await bankAccountService.GetById(
        context,
        newTransaction.bankAccountId
      );

      var type = await transactionType.GetById(
        context,
        newTransaction.transactionTypeid
      );

      BankAccountPostResponse response = new BankAccountPostResponse(
        bankAccount.number,
        transaction.day,
        transaction.value,
        type.type
      );


      return response;

    }


    public async Task<BankAccount> Update(
      DatabaseContext context,
      BankAccount bankAccount,
      int id
      )
    {

      var newBankAccount = await context.bankAccounts
        .FirstOrDefaultAsync(x => x.id == id);

      newBankAccount.clientId = bankAccount.clientId;
      newBankAccount.number = bankAccount.number;
      newBankAccount.iban = bankAccount.iban;
      newBankAccount.balance = bankAccount.balance;

      await context.SaveChangesAsync();

      return newBankAccount;
    }

    public async Task<BankAccount> UpdateBalance(
      DatabaseContext context,
      float value,
      string type,
      int id
    )
    {
      var newBankAccount = await context.bankAccounts
     .FirstOrDefaultAsync(x => x.id == id);

      if (type == "income")
      {
        newBankAccount.balance = newBankAccount.balance += value;
      }

      if (type == "outcome")
      {
        newBankAccount.balance = newBankAccount.balance -= value;
      }

      await context.SaveChangesAsync();

      return newBankAccount;
    }

    public async Task Delete(
      DatabaseContext context,
      int id)
    {
      BankAccount bankAccount = await context.bankAccounts
         .FirstOrDefaultAsync(x => x.id == id);

      context.bankAccounts.Remove(bankAccount);

      context.SaveChanges();

    }

  }
}