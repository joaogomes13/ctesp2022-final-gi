using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Service
{

  public class TransactionTypeService

  {

    public TransactionTypeService()
    {

    }

    public async Task<List<TransactionType>> GetAll(
      DatabaseContext context
    )
    {
      var type = await context.transactionTypes
          .ToListAsync();

      return type;

    }


    public async Task<TransactionType> GetById(
      DatabaseContext context,
      int id
    )
    {
      var type = await context.transactionTypes
          .AsNoTracking()
          .FirstOrDefaultAsync(x => x.id == id);

      return type;
    }

    public async Task<TransactionType> Save(
      DatabaseContext context,
      TransactionType type
      )
    {

      context.transactionTypes.Add(type);
      await context.SaveChangesAsync();
      return type;


    }


    public async Task<TransactionType> Update(
       DatabaseContext context,
      TransactionType transactionType,
      int id
      )
    {

      try
      {
        var expectedTransaction = await context.transactionTypes
        .FirstOrDefaultAsync(x => x.id == id);
      }
      catch (System.Exception)
      {

        throw;
      }

      var newTransactionType = await context.transactionTypes
        .FirstOrDefaultAsync(x => x.id == id);

      newTransactionType.type = transactionType.type;


      await context.SaveChangesAsync();

      return newTransactionType;
    }


    public async Task Delete(
      DatabaseContext context,
      int id)
    {
      try
      {
        TransactionType transactionType = await context.transactionTypes
        .FirstOrDefaultAsync(x => x.id == id);

        context.transactionTypes.Remove(transactionType);

        context.SaveChanges();
      }
      catch (System.Exception)
      {

        throw;
      }


    }

  }
}