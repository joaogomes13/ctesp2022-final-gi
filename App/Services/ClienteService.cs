using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Services
{

  public class ClientService

  {

    public ClientService()
    {

    }


    public async Task<List<Client>> GetAll(
      DatabaseContext context
    )
    {
      var clients = await context.clients
          .Include(x => x.bankAccounts)
          .ToListAsync();

      return clients;

    }


    public async Task<Client> GetById(
      DatabaseContext context,
      int id
    )
    {
      var client = await context.clients
          .Include(x => x.bankAccounts)
          .AsNoTracking()
          .FirstOrDefaultAsync(x => x.id == id);

      return client;
    }


    public async Task<Client> Save(
      DatabaseContext context,
      Client client
      )
    {
      try
      {
        context.clients.Add(client);
        await context.SaveChangesAsync();
        return client;
      }
      catch (System.Exception)
      {

        throw;
      }



    }


    public async Task<Client> Update(
       DatabaseContext context,
       Client client,
      int id
      )
    {
      try
      {
        var newClient = await context.clients
          .FirstOrDefaultAsync(x => x.id == id);

        newClient.name = client.name;
        newClient.address = client.address;
        newClient.contact = client.contact;

        await context.SaveChangesAsync();

        return newClient;
      }
      catch (System.Exception)
      {

        throw;
      }

    }


    public async Task Delete(
      DatabaseContext context,
      int id)
    {
      try
      {
        Client client = await context.clients
         .FirstOrDefaultAsync(x => x.id == id);

        context.clients.Remove(client);

        context.SaveChanges();
      }
      catch (System.Exception)
      {

        throw;
      }


    }

  }
}