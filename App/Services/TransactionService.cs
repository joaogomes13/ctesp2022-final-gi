using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using App.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Service
{

  public class TransactionService

  {

    public TransactionService()
    {

    }

    public async Task<List<Transaction>> GetAll(
      DatabaseContext context
    )
    {
      var transaction = await context.transactions
          .Include(x => x.bankAccount)
          .Include(x => x.transactionType)
          .ToListAsync();

      return transaction;

    }


    public async Task<Transaction> GetById(
      DatabaseContext context,
      int id
    )
    {
      var transaction = await context.transactions
          .Include(x => x.bankAccount)
          .Include(x => x.transactionType)
          .AsNoTracking()
          .FirstOrDefaultAsync(x => x.id == id);

      return transaction;
    }

    public async Task<Transaction> Save(
      DatabaseContext context,
      Transaction transaction
      )
    {
      try
      {
        var expectedAccount = await context.bankAccounts
        .AsNoTracking()
        .FirstOrDefaultAsync(x => x.id == transaction.bankAccountId);

      }
      catch (System.Exception)
      {

        throw new Exception("Account does not exists");
      }

      try
      {
        var expectedTransactionType = await context.transactionTypes
          .AsNoTracking()
          .FirstOrDefaultAsync(x => x.id == transaction.transactionTypeid);
      }
      catch (System.Exception)
      {

        throw new Exception("Transaction Type does not exists");
      }

      BankAccountService bankAccountService = new BankAccountService();
      TransactionTypeService transactionTypeService = new TransactionTypeService();


      context.transactions.Add(transaction);
      await context.SaveChangesAsync();

      var account = await context.bankAccounts
        .AsNoTracking()
        .FirstOrDefaultAsync(x => x.id == transaction.bankAccountId);

      TransactionType transactionType = await transactionTypeService.GetById(
        context,
        transaction.transactionTypeid
      );

      var updatedAccount = await bankAccountService.UpdateBalance(
        context,
        transaction.value,
        transactionType.type,
        transaction.bankAccountId
      );

      return transaction;
    }


    public async Task<Transaction> Update(
       DatabaseContext context,
      Transaction transaction,
      int id
      )
    {
      try
      {
        var expectedAccount = await context.bankAccounts
        .AsNoTracking()
        .FirstOrDefaultAsync(x => x.id == transaction.bankAccountId);
      }
      catch (System.Exception)
      {

        throw;
      }

      try
      {
        var expectedTransactionType = await context.transactionTypes
          .AsNoTracking()
          .FirstOrDefaultAsync(x => x.id == transaction.transactionTypeid);
      }
      catch (System.Exception)
      {

        throw;
      }

      try
      {
        var expectedTransaction = await context.transactions
        .FirstOrDefaultAsync(x => x.id == id);
      }
      catch (System.Exception)
      {

        throw;
      }

      var newTransaction = await context.transactions
        .FirstOrDefaultAsync(x => x.id == id);

      newTransaction.bankAccountId = transaction.bankAccountId;
      newTransaction.day = transaction.day;
      newTransaction.value = transaction.value;
      newTransaction.transactionTypeid = transaction.transactionTypeid;

      await context.SaveChangesAsync();

      return newTransaction;
    }


    public async Task Delete(
      DatabaseContext context,
      int id)
    {
      try
      {
        Transaction transaction = await context.transactions
        .FirstOrDefaultAsync(x => x.id == id);

        context.transactions.Remove(transaction);

        context.SaveChanges();
      }
      catch (System.Exception)
      {

        throw;
      }


    }

  }
}