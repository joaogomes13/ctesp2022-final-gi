namespace App.Dtos
{
  public class BankAccountPostResponse
  {
    public int accountNumber { get; set; }

    public string transactionDate { get; set; }

    public float transactionValue { get; set; }

    public string transactionType { get; set; }

    public BankAccountPostResponse(int accountNumber, string transactionDate, float transactionValue, string transactionType)
    {
      this.accountNumber = accountNumber;
      this.transactionDate = transactionDate;
      this.transactionValue = transactionValue;
      this.transactionType = transactionType;
    }

  }
}