namespace App.Dtos
{
  public class BankAccountGetResponse
  {
    public string titularName { get; set; }

    public string titularAddress { get; set; }

    public string titularContact { get; set; }

    public float balance { get; set; }

    public BankAccountGetResponse(string titularName, string titularAddress, string titularContact, float balance)
    {
      this.titularName = titularName;
      this.titularAddress = titularAddress;
      this.titularContact = titularContact;
      this.balance = balance;
    }

  }
}