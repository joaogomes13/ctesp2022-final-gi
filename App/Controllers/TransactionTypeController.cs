using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using App.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Controllers
{
  [ApiController]
  [Route("v1/transactionTypes")]
  public class TransactionTypeController : ControllerBase

  {
    TransactionTypeService service;
    public TransactionTypeController()
    {
      this.service = new TransactionTypeService();
    }

    /// <summary>
    ///  Get all the transactions types
    /// </summary>
    /// <response code="200">Sucess</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("")]
    public async Task<ActionResult<List<TransactionType>>> Index(
      [FromServices] DatabaseContext context
    )
    {
      var transactionTypes = await this.service.GetAll(
        context
      );

      return transactionTypes;

    }

    /// <summary>
    ///  Get a specific transaction types  by id
    /// </summary>
    /// <param name="id"> transaction types ID</param>
    /// <response code="200">Success</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<TransactionType>> GetById(
      [FromServices] DatabaseContext context,
      int id
    )
    {
      var transactionTypes = await this.service.GetById(
        context,
        id
      );

      return transactionTypes;
    }

    /// <summary>
    /// Create a transaction type
    /// </summary>
    /// <param name="client"> Transaction Type Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Trasaction  Type Model</response>
    /// <response code="500">Error</response>
    [HttpPost]
    [Route("")]
    public async Task<ActionResult<TransactionType>> Create(
      [FromServices] DatabaseContext context,
      [FromBody] TransactionType transactionType
      )
    {

      if (ModelState.IsValid)
      {
        TransactionType type = await this.service.Save(
          context,
          transactionType
        );

        return type;
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    /// <summary>
    /// Edit a specific transaction by ID
    /// </summary>
    /// <param name="id"> transaction ID</param>
    /// <param name="transaction"> transaction Model </param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid transaction Model</response>
    ///  <response code="404">Not Found</response>
    /// <response code="500">Error</response>
    [HttpPut]
    [Route("{id:int}")]
    public async Task<ActionResult<TransactionType>> Update(
      [FromServices] DatabaseContext context,
      [FromBody] TransactionType transactionType,
      int id
      )
    {
      var expectedTransactionType = await this.service.GetById(
        context,
        id
      );

      if (expectedTransactionType is null)
      {
        return BadRequest();
      }


      var newTransactionType = await this.service.Update(
        context,
        transactionType,
        id
      );

      return newTransactionType;

    }

    /// <summary>
    ///  Delete  a specific transaction by id
    /// </summary>
    /// <param name="id"> Transaction id</param>
    /// <response code="200">Sucess</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult> Delete(
      [FromServices] DatabaseContext context,
      int id)
    {
      TransactionType transactionType = await this.service.GetById(
        context,
        id
      );

      if (transactionType is null)
      {
        return NotFound();
      }

      await this.service.Delete(
        context,
        id
      );

      return StatusCode(200);
    }

  }
}