using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using App.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Controllers
{
  [ApiController]
  [Route("v1/transactions")]
  public class TransactionController : ControllerBase

  {
    TransactionService service;

    public TransactionController()
    {
      this.service = new TransactionService();
    }

    /// <summary>
    ///  Get all the transactions
    /// </summary>
    /// <response code="200">Sucess</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("")]
    public async Task<ActionResult<List<Transaction>>> Index(
      [FromServices] DatabaseContext context
    )
    {
      var transaction = await this.service.GetAll(
        context
      );

      return transaction;

    }

    /// <summary>
    ///  Get a specific transaction  by id
    /// </summary>
    /// <param name="id"> transaction ID</param>
    /// <response code="200">Success</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<Transaction>> GetById(
      [FromServices] DatabaseContext context,
      int id
    )
    {
      var transaction = await this.service.GetById(
        context,
        id
      );

      return transaction;
    }

    /// <summary>
    /// Create a transaction
    /// </summary>
    /// <param name="client"> Transaction Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Trasaction Model</response>
    /// <response code="500">Error</response>
    [HttpPost]
    [Route("")]
    public async Task<ActionResult<Transaction>> Create(
      [FromServices] DatabaseContext context,
      [FromBody] Transaction transaction
      )
    {
      if (ModelState.IsValid)
      {
        try
        {
          Transaction createdTransaction = await this.service.Save(
            context,
            transaction
          );

          return createdTransaction;

        }
        catch (System.Exception e)
        {

          return Problem(e.Message);
        }
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    /// <summary>
    /// Edit a specific transaction by ID
    /// </summary>
    /// <param name="id"> transaction ID</param>
    /// <param name="transaction"> transaction Model </param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid transaction Model</response>
    ///  <response code="404">Not Found</response>
    /// <response code="500">Error</response>
    [HttpPut]
    [Route("{id:int}")]
    public async Task<ActionResult<Transaction>> Update(
      [FromServices] DatabaseContext context,
      [FromBody] Transaction transaction,
      int id
      )
    {
      var updatedTransaction = await this.service.Update(
        context,
        transaction,
        id
      );

      return updatedTransaction;
    }

    /// <summary>
    ///  Delete  a specific transaction by id
    /// </summary>
    /// <param name="id"> Transaction id</param>
    /// <response code="200">Sucess</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult> Delete(
      [FromServices] DatabaseContext context,
      int id)
    {
      await this.service.Delete(
        context,
        id
      );

      return StatusCode(200);
    }

  }
}