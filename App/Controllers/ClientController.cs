using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using App.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace App.Controllers
{
  [ApiController]
  [Route("v1/clients")]
  public class ClientController : ControllerBase

  {
    ClientService service;
    public ClientController()
    {
      this.service = new ClientService();
    }

    /// <summary>
    ///  Get all the clients
    /// </summary>
    /// <response code="200">Sucess</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("")]
    public async Task<ActionResult<List<Client>>> Index(
      [FromServices] DatabaseContext context
    )
    {
      var clients = await this.service.GetAll(
        context
      );

      return clients;

    }

    /// <summary>
    ///  Get a specific client  by id
    /// </summary>
    /// <param name="id"> Client ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<Client>> GetById(
      [FromServices] DatabaseContext context,
      int id
    )
    {
      var client = await this.service.GetById(
        context,
        id
      );

      return client;
    }

    /// <summary>
    /// Create a client
    /// </summary>
    /// <param name="client"> Client Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Client Model</response>
    /// <response code="500">Error</response>
    [HttpPost]
    [Route("")]
    public async Task<ActionResult<Client>> Create(
      [FromServices] DatabaseContext context,
      [FromBody] Client client
      )
    {
      if (ModelState.IsValid)
      {
        Client createdClient = await this.service.Save(
          context,
          client
        );

        return createdClient;
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    /// <summary>
    /// Edit a specific Client by ID
    /// </summary>
    /// <param name="id"> Client ID</param>
    /// <param name="client"> Client Model </param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Client Model</response>
    ///  <response code="404">Not Found</response>
    /// <response code="500">Error</response>
    [HttpPut]
    [Route("{id:int}")]
    public async Task<ActionResult<Client>> Update(
      [FromServices] DatabaseContext context,
      [FromBody] Client client,
      int id
      )
    {
      try
      {
        var expectedClient = await this.service.GetById(
          context,
          id
        );

        if (id != expectedClient.id)
        {
          return BadRequest();
        }
      }
      catch (System.Exception)
      {

        return NotFound();
      }


      var newClient = await this.service.Update(
        context,
        client,
        id
      );

      return newClient;
    }

    /// <summary>
    ///  Delete  a specific Client by id
    /// </summary>
    /// <param name="id"> Client id</param>
    /// <response code="200">Sucess</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult> Delete(
      [FromServices] DatabaseContext context,
      int id)
    {
      await this.service.Delete(
        context,
        id
      );

      return StatusCode(200);
    }

  }
}