using System.Collections.Generic;
using System.Threading.Tasks;
using App.Contexts;
using App.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using App.Services;
using App.Dtos;

namespace App.Controllers
{
  [ApiController]
  [Route("v1/bankAccounts")]
  public class BankAccountController : ControllerBase

  {
    BankAccountService service;
    public BankAccountController()
    {
      this.service = new BankAccountService();
    }

    /// <summary>
    ///  Get all the bankaccounts
    /// </summary>
    /// <response code="200">Sucess</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("")]
    public async Task<ActionResult<List<BankAccount>>> Index(
      [FromServices] DatabaseContext context
    )
    {
      var accounts = await this.service.GetAll(context);

      return accounts;

    }

    /// <summary>
    ///  Get a specific bank account  by id
    /// </summary>
    /// <param name="id"> bank account ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("/BankAccount/{id:int}")]
    public async Task<ActionResult<BankAccountGetResponse>> Get(
      [FromServices] DatabaseContext context,
      int id
    )
    {
      var response = await this.service.Get(
        context,
        id
      );

      return response;
    }
    /// <summary>
    ///  Get a specific bank account  by id
    /// </summary>
    /// <param name="id"> bank account ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<BankAccount>> GetById(
      [FromServices] DatabaseContext context,
      int id
    )
    {
      var bankAccount = await this.service.GetById(
        context,
        id
      );

      return bankAccount;
    }

    /// <summary>
    ///  Get a specific bank account  by id
    /// </summary>
    /// <param name="id"> bank account ID</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Not found</response>
    /// <response code="500">Error</response>
    [HttpGet]
    [Route("clients/{id:int}")]
    public async Task<ActionResult<List<BankAccount>>> GetByClient(
     [FromServices] DatabaseContext context,
     int id
   )
    {
      var accounts = await this.service.GetByClient(
        context,
        id
      );

      return accounts;
    }

    /// <summary>
    /// Create a bank Account
    /// </summary>
    /// <param name="bankaccount"> Bank Account Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Bank Account Model</response>
    /// <response code="500">Error</response>
    [HttpPost]
    [Route("")]
    public async Task<ActionResult<BankAccount>> Create(
      [FromServices] DatabaseContext context,
      [FromBody] BankAccount bankAccount
      )
    {

      if (ModelState.IsValid)
      {
        var createdBankAccount = await this.service.Save(
          context,
          bankAccount
        );

        return createdBankAccount;
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    /// <summary>
    /// Create a bank Account
    /// </summary>
    /// <param name="bankaccount"> Bank Account Model</param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid Bank Account Model</response>
    /// <response code="500">Error</response>
    [HttpPost]
    [Route("/BankAccount")]
    public async Task<ActionResult<BankAccountPostResponse>> Post(
      [FromServices] DatabaseContext context,
      [FromBody] Transaction transaction
      )
    {

      if (ModelState.IsValid)
      {
        var createdTransaction = await this.service.Post(
          context,
          transaction
        );

        return createdTransaction;
      }
      else
      {
        return BadRequest(ModelState);
      }
    }

    /// <summary>
    /// Edit a specific BankAccount by ID
    /// </summary>
    /// <param name="id"> BankAccount ID</param>
    /// <param name="bankaccount"> BankAccount Model </param>
    /// <response code="200">Sucess</response>
    /// <response code="400">Invalid BankAccount Model</response>
    ///  <response code="404">Not Found</response>
    /// <response code="500">Error</response>
    [HttpPut]
    [Route("{id:int}")]
    public async Task<ActionResult<BankAccount>> Update(
      [FromServices] DatabaseContext context,
      [FromBody] BankAccount bankAccount,
      int id
      )
    {
      try
      {
        BankAccount expectedAccount = await this.service.GetById(
          context,
          id
        );

        if (id != expectedAccount.id)
        {
          return BadRequest();
        }

      }
      catch (System.Exception)
      {

        return NotFound();
      }

      var newBankAccount = await this.service.Update(
          context,
          bankAccount,
          id
      );

      return newBankAccount;
    }

    /// <summary>
    ///  Delete  a specific Bank Account by id
    /// </summary>
    /// <param name="id"> Bank Account id</param>
    /// <response code="200">Sucess</response>
    /// <response code="404">Not found</response>
    /// <response code="500">Error</response>
    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult> Delete(
      [FromServices] DatabaseContext context,
      int id)
    {
      BankAccount bankAccount = await this.service.GetById(
        context,
        id
      );

      if (bankAccount is null)
      {
        return NotFound();
      }

      await this.service.Delete(
        context,
        id
      );


      return StatusCode(200);
    }

  }
}